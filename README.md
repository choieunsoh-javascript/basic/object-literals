# Stop Using Conditional Statements Everywhere in JavaScript, Use an Object Literal instead

An Object Literal is one of the most popular and widely used pattern to define objects in JavaScript. It is a collection of key-value pairs. JavaScript being powerful, adds some additional functionalities to the simple objects through object literals.
