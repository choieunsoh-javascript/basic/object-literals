// Conditions: if-else
let country = "South Korea";
console.log("Conditions: if-else");
if (country === "South Korea") {
  console.log("Seoul");
} else if (country === "Thailand") {
  console.log("Bangkok");
}

// Conditions: switch-case
country = "Thailand";
console.log("\nConditions: switch-case");
switch (country) {
  case "South Korea":
    console.log("Seoul");
    break;
  case "Thailand":
    console.log("Bangkok");
    break;
  case "United States":
    console.log("Washington D.C.");
    break;
  default:
    console.log("Unknown");
    break;
}

// Object Literal
const countries = {
  Thailand: "Bangkok",
  "United States": "Washington D.C.",
  England: "London",
  Switzerland: "Bern",
  "South Korea": "Seoul",
  Japn: "Tokyo",
};

country = "United States";
console.log("\nObject Literal");
console.log(countries[country] ?? "Unknown");

country = "Germany";
console.log(countries[country] ?? "Unknown");
